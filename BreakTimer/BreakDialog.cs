﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BreakTimer
{
    public partial class BreakDialog : Form
    {
        public BreakDialog()
        {
            InitializeComponent();
        }

        private void buttonRestart_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void BreakDialog_Load(object sender, EventArgs e)
        {
            int y = Screen.PrimaryScreen.Bounds.Bottom - this.Height;
            int x = Screen.PrimaryScreen.Bounds.Right - this.Width;
            this.Location = new Point(x, y);
            this.TopMost = true;
        }
    }
}
