﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace BreakTimer
{
    public class BreakTimer: ApplicationContext
    {
        private IContainer _components;
        private NotifyIcon _notifyIcon;
        private PauseResumeTimer _timer;
        private int _intervalInMilliseconds = 60 * 60 * 1000;

        public BreakTimer()
        {
            InitializeContext();
        }

        private void InitializeContext()
        {
            _components = new Container();
            _notifyIcon = new NotifyIcon()
            {
                ContextMenuStrip = new ContextMenuStrip(),
                Icon = Icons.StopIcon,
                Text = "Break Timer",
                Visible = true
            };

            _notifyIcon.Click += (sender, args) =>
            {
                AddContextMenuItems();
                var methodInfo = typeof(NotifyIcon).GetMethod("ShowContextMenu", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                methodInfo.Invoke(_notifyIcon, null);
            };

            _timer = new PauseResumeTimer(_components, () =>
            {
                _notifyIcon.Icon = Icons.FinishedIcon;
                var breakDialog = new BreakDialog();
                switch (breakDialog.ShowDialog())
                {
                    case DialogResult.OK:
                         _notifyIcon.Icon = Icons.PlayIcon;
                        _timer.Start(_intervalInMilliseconds);
                        break;
                    default:
                        _notifyIcon.Icon = Icons.StopIcon;
                        break;
                }
            });

           
        }

        private void AddContextMenuItems()
        {
            _notifyIcon.ContextMenuStrip.Items.Clear();

            if (_timer.IsPaused)
            {
                _notifyIcon.ContextMenuStrip.Items.Add(
                    String.Format("Resume ({0})", _timer.GetRemainingTime()), 
                    null, 
                    (sender, args) => {
                        _notifyIcon.Icon = Icons.PlayIcon;
                        _timer.Resume();
                    });
            }
            else if (!_timer.IsRunning)
            {
                _notifyIcon.ContextMenuStrip.Items.Add("Start", null, (sender, args) =>
                {
                    _notifyIcon.Icon = Icons.PlayIcon;
                    _timer.Start(_intervalInMilliseconds);
                });
            }
            else
            {
                _notifyIcon.ContextMenuStrip.Items.Add(
                    String.Format("Pause ({0})", _timer.GetRemainingTime()), 
                    null, 
                    (sender, args) => {
                        _notifyIcon.Icon = Icons.PauseIcon;
                        _timer.Pause();
                    });
            }
            
            _notifyIcon.ContextMenuStrip.Items.Add("Stop", null, (sender, args) =>
            {
                _notifyIcon.Icon = Icons.StopIcon;
                _timer.Stop();
            });

            _notifyIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            _notifyIcon.ContextMenuStrip.Items.Add("Configure...", null, (sender, args) =>
            {
                var settings = new SettingsDialog((_intervalInMilliseconds / 1000) / 60);
                settings.ShowDialog();
                _intervalInMilliseconds = int.Parse(settings.Interval) * 60 * 1000;
            });
            _notifyIcon.ContextMenuStrip.Items.Add("Exit", null, (sender, args) =>
            {
                ExitThread();
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _components != null)
            {
                _components.Dispose();
            }
        }
    }
}
