﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BreakTimer
{
    public static class Icons
    {
        private static Icon stop_icon = new Icon("images/clock_stop.ico");
        private static Icon play_icon = new Icon("images/clock_play.ico");
        private static Icon pause_icon = new Icon("images/clock_pause.ico");
        private static Icon finished_icon = new Icon("images/clock_error.ico");

        public static Icon StopIcon
        {
            get { return stop_icon; }
        }

        public static Icon PlayIcon
        {
            get { return play_icon; }
        }

        public static Icon PauseIcon
        {
            get { return pause_icon; }
        }

        public static Icon FinishedIcon
        {
            get { return finished_icon; }
        }
    }
}
