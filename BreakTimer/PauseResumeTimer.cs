﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace BreakTimer
{
    public class PauseResumeTimer
    {
        private DateTime _startTime;
        private DateTime _endTime;
        private TimeSpan _timeRemaining;
        private Timer _timer;
        private bool _isPaused = false;

        public PauseResumeTimer(IContainer container, Action timerFinished)
        {
            _timer = new Timer(container);
            _timer.Tick += (sender, args) => 
            { 
                _timer.Stop();
                timerFinished();
            };
        }

        public void Start(int interval)
        {
            _isPaused = false;
            _timer.Interval = interval;
            _startTime = DateTime.Now;
            _endTime = _startTime.AddMilliseconds(_timer.Interval);
            _timer.Start();
        }

        public void Stop()
        {
            _isPaused = false;
            _timer.Stop();
        }

        public void Pause()
        {
            _isPaused = true;
            _timeRemaining = _endTime - DateTime.Now;
            _timer.Stop();
        }

        public void Resume()
        {
            _isPaused = false;
            _timer.Interval = (int)_timeRemaining.TotalMilliseconds;
            _startTime = DateTime.Now;
            _endTime = _startTime.AddMilliseconds(_timer.Interval);
            _timer.Start();
        }

        public bool IsRunning
        {
            get
            {
                return _isPaused || _timer.Enabled;
            }
        }

        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }
        }

        public string GetRemainingTime()
        {
            if (!_isPaused)
            {
                _timeRemaining = _endTime - DateTime.Now;
            }

            return _timeRemaining.ToString(@"hh\:mm\:ss");
        }
    }
}
