﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BreakTimer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                var appContext = new BreakTimer();
                Application.Run(appContext);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Failed to start tray application",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
