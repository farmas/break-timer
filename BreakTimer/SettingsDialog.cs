﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BreakTimer
{
    public partial class SettingsDialog : Form
    {
        public SettingsDialog(int interval)
        {
            InitializeComponent();
            this.textBoxInterval.Text = interval.ToString();
        }

        public string Interval
        {
            get
            {
                return textBoxInterval.Text;
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void SettingsDialog_Load(object sender, EventArgs e)
        {
            int y = Screen.PrimaryScreen.Bounds.Top + this.Height;
            int x = Screen.PrimaryScreen.Bounds.Right - this.Width;
            this.Location = new Point(x, y);
            this.TopMost = true;
        }
    }
}
